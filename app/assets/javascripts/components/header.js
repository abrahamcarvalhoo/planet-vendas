$(function(){
  var headerHeight = 0;
  var header = $('.header');

  if ($(window).width() >= 992) {
    headerHeight =  header.outerHeight();

    $(window).scroll(function() {
      if ($(this).scrollTop() > 80) {
        header.addClass('is-active');
      } else {
        header.removeClass('is-active');
      }
    });
  } else {
    $('.header-menu__item').click(function(event) {
      header.removeClass('is-active');
      $('.navigation').removeClass('is-active');
    });
  }

  $('.navigation').click(function(event) {
    header.toggleClass('is-active');
    $(this).toggleClass('is-active');
  });

  $('.header__logo, .header-menu__item, .hero__banner-button, .platform__button').click(function(event) {
    event.preventDefault();
    var target = $(this).attr('href');

    $('html, body').animate({ scrollTop: $(target).offset().top - headerHeight }, 750);
  });

  $('.steps__actions-item').click(function(event) {
    event.preventDefault();
    var target = $(this).attr('href');

    $(this).siblings().removeClass('is-active');
    $(this).addClass('is-active');

    $('.steps__tabs-item').siblings().removeClass('is-active');
    $('.steps__tabs-item'+target).addClass('is-active');

    $('html, body').animate({ scrollTop: $('#steps').offset().top - headerHeight + 50 }, 500);
  });
});

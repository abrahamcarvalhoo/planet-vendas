$(function(){
  var discoveryList = $('.discovery__list').swiper({
    speed: 500,
    touchRatio: 0.5,
    slidesPerView: 1,
    autoHeight: true,
    keyboardControl: true,
    paginationClickable: true,
    prevButton: '.discovery__nav--prev',
    nextButton: '.discovery__nav--next',
    pagination: '.discovery .swiper-pagination'
  });
});
